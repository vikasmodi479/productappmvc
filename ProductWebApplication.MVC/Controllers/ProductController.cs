﻿using Microsoft.AspNetCore.Mvc;
using ProductWebApplication.MVC.ExceptionAttribute;
using ProductWebApplication.MVC.Exceptions;
using ProductWebApplication.MVC.Models;
using ProductWebApplication.MVC.Services;

namespace ProductWebApplication.MVC.Controllers
{
    [ExceptionHandler]
    public class ProductController : Controller
    {
        readonly IProductService _productServices;
        public ProductController(IProductService productServices)
        {
            _productServices = productServices;
        }
        [HttpGet]
        public ActionResult Index()
        {
            List<Product> productsList = _productServices.GetProduct();
            return View(productsList);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Product product)
        {
            int productAddStatus = _productServices.AddProduct(product);
            if (productAddStatus == 1)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return Redirect("Create");
            }
        }
        [HttpGet]   
        public ActionResult Delete(int id)
        {
            Product? productDetailsToDelete = _productServices.GetProductById(id);
            return View(productDetailsToDelete);
        }
        [HttpPost]
        public ActionResult Delete(Product product)
        {
            int productDeleteStatus = _productServices.DeleteProduct(product.Id);
            if (productDeleteStatus == 1)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }   
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Product? product = _productServices.GetProductById(id);
            return View(product);
        }
        [HttpPost]
        public ActionResult Edit(Product product)
        {
            int UpdateStatus = _productServices.UpdateProduct(product);
            if (UpdateStatus == 1)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(product);
            }
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            Product? productDetails = _productServices.GetProductById(id);
            return View(productDetails);
        }
    }
}
