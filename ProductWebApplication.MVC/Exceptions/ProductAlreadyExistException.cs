﻿namespace ProductWebApplication.MVC.Exceptions
{
    public class ProductAlreadyExistException:Exception
    {
        public ProductAlreadyExistException()
        {

        }
        public ProductAlreadyExistException(string msg):base(msg)
        {

        }
    }
}
