﻿using ProductWebApplication.MVC.Exceptions;
using ProductWebApplication.MVC.Models;
using ProductWebApplication.MVC.Repository;

namespace ProductWebApplication.MVC.Services
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool AddUser(User user)
        {
            User? _user = _userRepository.GetUserByName(user.Name);
            if (_user == null)
            {
                return _userRepository.AddUser(user);
            }
            else
            {
                throw new ProductAlreadyExistException($"{user.Name} User Already Exist !!");
            }
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public User Login(LoginUser loginUser)
        {
            User user = _userRepository.Login(loginUser.Name, loginUser.Password);
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new UserCredentialInvalidException($"{loginUser.Name} and other details are invalid");
            }
        }
    }
}
